/*===========================================================================*/
/*
 * This file is part of
 * libneuro - a C++ library for signals in the Neuro format
 *
 * Copyright (C) 2006, 2007, 2008  Elaine Tsiang YueLien
 *
 * libneuro is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see http://www.gnu.org/licenses, or write
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA
 *
 *===========================================================================*/
/** @file Thread.C                                                           */
/*                                                                           */
/* Examples::Thread implementation                                           */
/*                                                                           */
/*===========================================================================*/
#ifdef __GNUG__
#pragma implementation
#endif

#include	"Thread.H"

extern	"C"
{
#include	<sched.h>   
}

namespace	Examples
{
  using namespace	Ogg;

  void
  Thread::start()
  {
    pthread_attr_init(&attr);	// default values

    // set some scheduling policy
    pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);

    thread = new pthread_t;

    if ( 0 != 
	 pthread_create(thread
			,&attr
			,runThread
			,static_cast<void *>(this)
			)
	 )
      {
	delete thread;
	throw Thread::FailedToSpawnThread();
      }
  }

  void
  Thread::finish()
  {
    void *	pretval;
    
    if ( 0 !=
	 pthread_join(*thread
		      ,&pretval
		      )
	 )
      {
	throw Thread::FailedToFinishThread();
      }
  }

  void *	runThread(
			  void * thrd
			  )
  {
    Thread *	thread = static_cast<Thread *>(thrd);

    thread->run();
    return(0);
  }
}
