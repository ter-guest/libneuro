/*===========================================================================*/
/*
 * This file is part of
 * libneuro - a C++ library for signals in the Neuro format
 *
 * Copyright (C) 2006, 2007, 2008 , 2009-2014 Elaine Tsiang YueLien
 *
 * libneuro is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see http://www.gnu.org/licenses, or write
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA
 *
 *===========================================================================*/
/** @file NeuroImpl.C                                                        *
 *                                                                           *
 *  Neuro::Neuro implementation                                              *
 *                                                                           */
/*===========================================================================*/
#ifdef __GNUG__
#pragma implementation "Neuro.H"
#pragma implementation "NeuroImpl.H"
#endif

#include	<NeuroImpl.H>
#include	<Ogg/Debug.H>

extern "C" 
{
#include	<stdlib.h>
#include	<string.h>
}


namespace	Neuro
{
  using	namespace Ogg;
  using namespace ALingA;

  //--iFrames-------------------------------------------------------------------------------

  NeuroImpl::iFramesImpl::iFramesImpl(
				      NeuroImpl &	neuro
				      ,Signal *		signal
				      )
    : neuro_(neuro)
    , reader_(0)
    , writer_(0)
    , signal_(signal)
    , numFrames_(0)
    , currentPacketSize_(0)
    , totalGrans_(0)
  {
    
    if ( neuro_.writing_ )
      {
	if ( signal_ )
	  signal_->beginWrite("Neuro");
	writer_ = &neuro_.intf_->writer();
	neuro_.writeHeaders(*writer_);
      }
    else
      {
	reader_ = &neuro_.intf_->reader();
	neuro_.doneParsingHeaders_ = false;
	neuro_.readHeaders(*reader_);
      }

    gransPerFrame_ = neuro_.manifold_.gransPerFrame();
    frameSize_ =  gransPerFrame_ * neuro_.encode_.dataSize();
    packetSize_ = neuro_.framesPerPacket_ * frameSize_;
    if ( writer_ )
      {
	(*writer_).dataSize(packetSize_);
	frames_ = reinterpret_cast<char *>((*writer_)->data());
      }
    else if ( reader_ )
      {
	if ( !neuro_.dataIsBigEndian_ )
	  neuro_.encode_.decode(
				(*reader_)->data()
				,(*reader_)->size()
				);
	frames_ = reinterpret_cast<char *>((*reader_)->data());
	if ( (*reader_)->size() != packetSize_ )
	  throw Neuro::InconsistentPacketSize(packetSize_
					      ,(*reader_)->size()
					      );
      }
  }

  Neuro::iFrames::~iFrames()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);

    debug(true
	  ,"~iFrames: numFrames ", i->numFrames_
	  );

    if ( i->reader_ )
      {
	delete i->reader_;
      }
    else if ( i->writer_ )
      {
	i->currentPacketSize_ += i->frameSize_;
	i->numFrames_++;
	i->totalGrans_ += i->gransPerFrame_;
	(*i->writer_).dataSize(i->currentPacketSize_);
	if ( !i->neuro_.dataIsBigEndian_ )
	  i->neuro_.encode_.encode((*i->writer_)->data()
				   ,(*i->writer_)->size()
				   );
	(*i->writer_).granulePosition(i->basePosition());
	delete i->writer_;
	if ( i->signal_ )
	  i->signal_->endWrite();
      }

  }

  size_t
  Neuro::iFrames::gransPerFrame()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return(i->gransPerFrame_);
  }

  size_t
  Neuro::iFrames::dataSize()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return(i->neuro_.encode_.dataSize());
  }

  size_t
  Neuro::iFrames::frameSize()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return(i->frameSize_);
  }

  size_t
  Neuro::iFrames::frameNo()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return(i->numFrames_);
  }

  bool
  Neuro::iFrames::dataFloat()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return(i->neuro_.encode_.floating());
  }

  bool
  Neuro::iFrames::dataSigned()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return(i->neuro_.encode_.isSigned());
  }

  Neuro::iFrames &
  Neuro::iFrames::operator ++ ()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    i->currentPacketSize_ += i->frameSize_;
    i->frames_ += i->frameSize_;
    i->numFrames_++;
    i->totalGrans_ += i->gransPerFrame_;

    if ( i->currentPacketSize_ >= i->packetSize_ )
      {
	// exhausted this packet
	if ( i->writer_ )
	  {
	    Logical::Writer & writer = *(i->writer_);
	    // code this packet
	    if ( !i->neuro_.dataIsBigEndian_ )
	      i->neuro_.encode_.encode(writer->data()
				       ,writer->size()
				       );
	    writer.granulePosition( i->basePosition() );
	    ++writer;
	    i->currentPacketSize_ = 0;
	    i->frames_ = reinterpret_cast<char *>(writer->data());
	  }
	else if ( i->reader_ )
	  {
	    Logical::Reader & reader = *i->reader_;

	    if ( !reader.ending() )
	      {
		++reader;
		i->currentPacketSize_ = 0;
		i->frames_ = reinterpret_cast<char *>(reader->data());
		// decode this packet
		if ( !i->neuro_.dataIsBigEndian_ )
		  i->neuro_.encode_.decode(
					   reader->data()
					   ,reader->size()
					   );
		if ( reader->size() != i->packetSize_ )
		  {
		    if ( reader.ending() )
		      i->packetSize_ = reader->size();
		    else
		      // throw here after packet is decoded anyway
		      // so user can choose to ignore with no foul
		      throw Neuro::InconsistentPacketSize(i->packetSize_
							  ,reader->size()
						      );
		  }
	      }
	  }
      }

    return(*this);
  }
	
  bool
  Neuro::iFrames::ended()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return ( (*i->reader_).ending() 
	     &&
	     ( i->currentPacketSize_ >= i->packetSize_ )
	     );
  }
  
  void *
  Neuro::iFrames::frame()
  {
    NeuroImpl::iFramesImpl * i
      = static_cast<NeuroImpl::iFramesImpl *>(this);
    
    return( i->frames_ );
  }

  void
  Neuro::iFrames::frame(
			void *	received
			,size_t	siz
			)
  {
    if ( dataSize() < siz )
      siz = dataSize();

    memcpy(frame()
	   ,received
	   ,siz
	   );
  }

  //--Neuro--------------------------------------------------------------------------------

  Neuro::Neuro(
	       Transport &	transport
	       ,long		track
	       ,const Manifold &manifold
	       ,Encoding	encoding
	       ,long		framesPerPacket
	       )
    : Logical(transport
	      ,track
	      )
  {
    Encode enc(encoding);
    size_t pktsz = (framesPerPacket * manifold.gransPerFrame() * enc.dataSize());

    if ( pktsz < NeuroImpl::minimumPacketSize_
	 )
      throw PacketSizeTooSmall(NeuroImpl::minimumPacketSize_
			       ,pktsz
			       );

    NeuroImpl *impl = new NeuroImpl(
				    manifold
				    , enc
				    , framesPerPacket
				    );

    impl->intf_ = this;
    impl_ = impl;
  }

  NeuroImpl::NeuroImpl(
		       const Manifold &	manifold
		       ,Encode &	encode
		       ,long		framesPerPacket
		       )
    : writing_(true)
    , xmlns_("http://www.ihear.com/dtds/Neuro.dtd")
    , manifold_(manifold)
    , encode_(encode)
    , dataIsBigEndian_(Encode::hostIsBigEndian())
    , framesPerPacket_(framesPerPacket)
    , doneParsingHeaders_(false)
    , metas_()
  {
  }

  Neuro::Neuro(
	       Transport &	transport
	       )
    : Logical(transport)
  {
    NeuroImpl *impl = new NeuroImpl();

    impl->intf_ = this;
    impl_ = impl;
  }

  Neuro::Neuro(
	       long		track
	       ,Transport &	transport
	       )
    : Logical(track, transport)
  {
    NeuroImpl *impl = new NeuroImpl();

    impl->intf_ = this;
    impl_ = impl;
  }

  NeuroImpl::NeuroImpl()
    : writing_(false)
    , xmlns_()
    , manifold_(0)
    , encode_()
    , dataIsBigEndian_(Encode::hostIsBigEndian())
    , framesPerPacket_(0)
    , doneParsingHeaders_(0)
    , metas_()
  {}

  Neuro::~Neuro()
  {
    if ( impl_ )
      {
	NeuroImpl * impl = static_cast<NeuroImpl *>(impl_);
	delete impl;
	impl_ = 0;
      }
  }

  long
  Neuro::track() const
  {
    if ( !serialNo() )
      throw NotReadingAnyNeuro();

    return(serialNo());
  }

  Manifold &
  Neuro::manifold()
  {
    NeuroImpl * impl = static_cast< NeuroImpl *>(impl_);

    if ( !serialNo() )
      throw NotReadingAnyNeuro();

    return(impl->manifold_);
  }

  size_t
  Neuro::framesPerPacket() const
  {
    const NeuroImpl * impl = static_cast<const NeuroImpl *>(impl_);

    if ( !serialNo() )
      throw NotReadingAnyNeuro();

    return(impl->framesPerPacket_);
  }

  Metas &
  Neuro::metas() 
  {
    NeuroImpl * impl = static_cast< NeuroImpl *>(impl_);

    return(impl->metas_);
  }

  void
  Neuro::dataIsBigEndian(bool	yes)
  {
    NeuroImpl * impl = static_cast<NeuroImpl *>(impl_);

    impl->dataIsBigEndian_ = yes;
  }

  bool
  Neuro::dataIsBigEndian() const
  {
    const NeuroImpl * impl = static_cast<const NeuroImpl *>(impl_);

    return(impl->dataIsBigEndian_);
  }

  Neuro::iFrames &
  Neuro::write(
	       Signal *	signal
	       )
  {
    NeuroImpl * impl = static_cast<NeuroImpl *>(impl_);

    // Logical will throw a fit if write is called twice
    iFrames * iframes = new NeuroImpl::iFramesImpl(*impl
						   ,signal
						   );
	
    return(*iframes);
  }
    
  Neuro::iFrames &
  Neuro::read()
  {
    NeuroImpl * impl = static_cast<NeuroImpl *>(impl_);

    // Logical will throw a fit if read is called twice
    iFrames * iframes = new NeuroImpl::iFramesImpl(*impl);
	
    return(*iframes);
  }

  bool
  Neuro::selectCallback(
			Packet &	pkt
			)
  {
    char * header = static_cast<char *>(pkt.data());

    if( 
       !strncmp(header, "Neuro", 5)
       &&
       ( header[5] == '\0' )
       &&
       ( header[6] == '\0' )
       &&
       ( header[7] == '\0' )
	)
      {
	Encode			enc(Unsigned16Bit);
	unsigned short uMajor = *reinterpret_cast<unsigned short *>(header+8);
	unsigned short uMinor = *reinterpret_cast<unsigned short *>(header+10);
	enc.decode(&uMajor
		   ,sizeof(unsigned short)
		   );
	enc.decode(&uMinor
		   ,sizeof(unsigned short)
		   );
	if ( (uMajor != NeuroImpl::dtdMajor_)
	     ||
	     (uMinor != NeuroImpl::dtdMinor_)
	     )
	  throw Neuro::InconsistentDTDversion(
					       uMajor
					       ,uMinor
					       ,NeuroImpl::dtdMajor_
					       ,NeuroImpl::dtdMinor_
					       );
	return(true);
      }
    return(false);
  }

  void
  NeuroImpl::writeHeaders(
			  Logical::Writer &	writer
			  )
  {
    // write header packets

    char * const	data = reinterpret_cast<char *>(writer->data());
    size_t		off = 12;

    // Ogg-format preamble in header 0
    data[0] = 'N';data[1]='e';data[2]='u';data[3]='r';data[4]='o';
    data[5] = 0;data[6]=0;data[7]=0;

    // Encode will impose big-endian order
    Encode		enc(Unsigned16Bit);
    unsigned short *	u = reinterpret_cast<unsigned short *>(data+8);
    *u = dtdMajor_;
    enc.encode(u
	       ,sizeof(unsigned short)
	       );
    ++u;
    *u = dtdMinor_;
    enc.encode(u
	       ,sizeof(unsigned short)
	       );


    ALingA::XMLwriter xmlWriter(
				"Neuro"
				,dtdMajor_
				,dtdMinor_
				);
    {
      ALingA::XMLwriter neuroWriter(
				    xmlWriter
				    ,"Neuro"
				    );

      neuroWriter.manifold(manifold_);

      neuroWriter.xmlns(xmlns_);

      neuroWriter.endAttributes();

      {
	ALingA::XMLwriter frameTypeWriter(
					  neuroWriter
					  ,"FrameType"
					  );
    
	frameTypeWriter.attribute(
				  "encoding"
				  ,encode_.inText()
				  );
	
	ALingA::SizeAttribute	fpp("framesPerPacket"
				    ,framesPerPacket_
				    );
	frameTypeWriter.attribute(
				  fpp.name
				  ,fpp.value
				  );
	frameTypeWriter.endAttributes();

	if ( !metas_.empty() )
	  {
	    // We break out header 0 with frameType not closed
	    // if we have metas
	    writer.dataSize(off+frameTypeWriter.textSize());
	    frameTypeWriter.write(
				  reinterpret_cast<char *>(writer->data())+off
				  );
	    // first header, always flushed
	    ++writer;
	    off = 0;

	    ALingA::XMLwriter metasWriter(
					  frameTypeWriter
					  ,"Metas"
					  );
	    metasWriter.endAttributes();
	    metasWriter.metas(metas_);
	  }
	// metas tag closed
      }
      // frameType tag closed
    }
    // Neuro tag closed
    writer.dataSize(off+xmlWriter.textSize());
    xmlWriter.write(
		    reinterpret_cast<char *>(writer->data())+off
		    );

    // Done writing headers, get the first frames to write
    // flush next head
    writer.flush();
    ++writer;
    writer.beginData();
  }
    
  void
  NeuroImpl::readHeaders(
			 Logical::Reader &	reader
			 )
  {
    // parse header packets
    XMLparser & xmlParser = *(new XMLparser(
					    *this
					    ));

    // skip header 0 Ogg-format preamble already checked in selectCallBack
    size_t	off = 12;
    xmlParser.parse(reinterpret_cast<char *>(reader->data())+off
		    ,reader->size()-off
		    );
    ++reader;
    while ( !doneParsingHeaders_ )
      {
	xmlParser.parse(reader->data()
			,reader->size()
			);
	++reader;
      }

    delete &xmlParser;
  }

  void
  XMLparser::on_start_element(
			      const Glib::ustring & name
			      ,const xmlpp::SaxParser::AttributeList& attributes
			      )
  {
    if ( name == "Neuro" )
      new NeuroXMLparser(top()		// this
			 ,neuroImpl_
			 ,attributes
			 );
    else
      throw XMLparser::ContextCorruption("Neuro"
					 ,name
					 );

  }

  NeuroXMLparser::NeuroXMLparser(
				 XMLparser &		xmlParser
				 ,NeuroImpl &		neuroImpl
				 ,const xmlpp::SaxParser::AttributeList& attributes
				 )
    : ALingA::XMLparser("Neuro"
			,&xmlParser
			)
      , neuroImpl_(neuroImpl)
  {
    for ( xmlpp::SaxParser::AttributeList::const_iterator i = attributes.begin()
	    ;
	  i != attributes.end()
	    ;
	  ++i
	  )
      {
	 if ( (*i).name == "dtdVersion" )
	  {
	    // already checked - just let it go
	  }
	 else if ( (*i).name == "xmlns" )
	  {
	    neuroImpl_.xmlns_ = (*i).value;
	  }
	 else if ( (*i).name != "xmlns:xlink" )
	  {
	    neuroImpl_.manifold_(*i);
	  }
      }
  }

  void
  NeuroXMLparser::on_end_element(
				 const Glib::ustring& 	name
				 )
  {
    neuroImpl_.doneParsingHeaders_ = true;
  }

  void
  NeuroXMLparser::on_start_element(
				   const Glib::ustring & name
				   ,const xmlpp::SaxParser::AttributeList& attributes
				   )
  {
    if ( name == "FrameType" )
      new FrameTypeXMLparser(top()
			     ,neuroImpl_
			     ,attributes
			     );
    else
      throw XMLparser::ContextCorruption("FrameType"
					 ,name
					 );
  }

  FrameTypeXMLparser::FrameTypeXMLparser(
					 XMLparser &	neuroXmlParser
					 ,NeuroImpl &		neuroImpl
					 ,const xmlpp::SaxParser::AttributeList& attributes
					 )
    : ALingA::XMLparser("FrameType"
			,&neuroXmlParser
			)
      , neuroImpl_(neuroImpl)
  {
    for ( xmlpp::SaxParser::AttributeList::const_iterator i = attributes.begin()
	    ;
	  i != attributes.end()
	    ;
	  ++i
	  )
      {
	if ( i->name == "encoding" )
	  {
	    Encode enc(i->value);
	    neuroImpl_.encode_ = enc;
	  }
	else if ( i->name == "framesPerPacket" )
	  {
	    long fpp = atol(i->value.c_str());
	    if ( fpp <= 0 )
	      throw ALingA::XMLparser::BadAttributeValue(*i);
	    neuroImpl_.framesPerPacket_ = size_t(fpp);
	  }
      }
  }

  void
  FrameTypeXMLparser::on_start_element(
				       const Glib::ustring & name
				       ,const xmlpp::SaxParser::AttributeList& attributes
				       )
  {
    if ( name == "Metas" )
      {
	new ALingA::MetasXMLparser(top()
				   ,neuroImpl_.metas_
				   );
      }
    else
      throw XMLparser::ContextCorruption("Metas"
					 ,name
					 );
  }

  Neuro::NotReadingAnyNeuro::NotReadingAnyNeuro() throw()
    : Exception("Currently not reading any Neuro stream")
  {}

  Neuro::AlreadyReadingNeuro::AlreadyReadingNeuro() throw()
    : Exception("Currently not reading any Neuro stream")
  {}

  Neuro::AlreadyWritingNeuro::AlreadyWritingNeuro() throw()
    : Exception("Currently not reading any Neuro stream")
  {}

  Neuro::InconsistentDTDversion::InconsistentDTDversion(
							unsigned short	readMajor
							,unsigned short	readMinor
							,unsigned short	dtdMajor
							,unsigned short	dtdMinor
							) throw()
    : Exception("Stream's version ")
  {
    *this << readMajor << "." << readMinor
	  << " does not match Neuro implmentation's "
	  << dtdMajor << "." << dtdMinor << std::endl;
  }

  Neuro::PacketSizeTooSmall::PacketSizeTooSmall(
						size_t	minsz
						,size_t	actual
						) throw()
    : Exception("Packet size ")
  {
    *this << actual << " less than minimum of " << minsz << std::endl;
  }

  Neuro::InconsistentPacketSize::InconsistentPacketSize(
							size_t	advertised
							,size_t	actual
							) throw()
    : Exception("Actual packet size ")
  {
    *this << actual << " not equal to advertised " << advertised << std::endl;
  }

}
