/*===========================================================================*/
/*
 * This file is part of
 * libneuro - a C++ library for signals in the Neuro format
 *
 * Copyright (C) 2006, 2007, 2008 , 2009-2014 Elaine Tsiang YueLien
 *
 * libneuro is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, see http://www.gnu.org/licenses, or write
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301, USA
 *
 *===========================================================================*/
/** @file Neuro.H                                                            *
 *                                                                           *
 *  Neuro::Neuro interface                                                   *
 *                                                                           */
/*===========================================================================*/
#ifndef	NeuroInterface
#define	NeuroInterface

#ifdef __GNUG__
#pragma interface
#endif

#include	<Neuro/Neuro.H>
#include	<ALingA/Encode.H>
#include	<ALingA/Manifold.H>
#include	<ALingA/Metas.H>
#include	<ALingA/Signal.H>
#include	<Ogg/Ogg.H>


namespace	Neuro
{
  /// @defgroup neuro		Neuro Interface
  //@{

  class Neuro : public Ogg::Logical
  {
  private:
    void *			impl_;

    Neuro(const Neuro &);

    Neuro &
    operator=(const Neuro &);

  public:
    /// @brief Iterator of frames in a Neuro stream
    ///
    /// @ingroup derivables
    ///
    /// Do not derive from iFrames
    class iFrames
    {
    private:
      iFrames(const iFrames &);
      
      iFrames &
      operator=(const iFrames &);
      
    protected:
      iFrames(){};

    public:
      /// @brief Ends reading or writing this Neuro stream
      ///
      /// Closes the Neuro stream
      ~iFrames();
    
      size_t
      gransPerFrame();
    
      size_t
      dataSize();

      size_t
      frameSize();

      bool
      dataFloat();

      bool
      dataSigned();

      iFrames &
      operator ++ ();

      ///@brief	Only meaningful when reading
      bool
      ended();

      size_t
      frameNo();

      void
      frame(void *
	    ,size_t
	    );

      void *
      frame();
    };


    ///@brief Construct for writing.
    ///
    /// Note that the argument parameters are not changeable,
    /// except by instantiating a different Neuro.
    Neuro(
	  Ogg::Transport &
	  ,long				track
	  ,const ALingA::Manifold &
	  ,ALingA::Encoding
	  ,long				framesPerPacket
	  );
    
    ///@brief Construct for reading.
    ///
    /// Note that the track, manifold, encoding and framesPerPacket
    /// are variable according to the received Neuro data.
    Neuro(
	  Ogg::Transport &
	  );

    ///@brief Construct for reading a particular track
    Neuro(
	  long		track
	  ,Ogg::Transport &
	  );

    virtual
    ~Neuro();

    /// @brief track of stream being read
    ///
    /// may throw NotReadingAnyNeuro
    long
    track() const;

    /// @brief Get manifold of stream being read
    ALingA::Manifold &
    manifold();

    /// @brief Get framesPerPacket of stream being read
    ///
    /// may throw NotReadingAnyNeuro
    size_t
    framesPerPacket() const;

    /// @brief Metas of stream being read or written
    ///
    ALingA::Metas &
    metas();

    /// @brief Set endianness of data to be written or read.
    ///
    /// Default is same as executing host's endianness.
    void
    dataIsBigEndian(bool yes);

    /// @brief Returns endianness of data to be written or read.
    bool
    dataIsBigEndian() const;

    class iFrames;

    /// @brief Write new Neuro stream
    iFrames &
    write(
	  ALingA::Signal *	signal = 0
	  );

    /// @brief Read new Neuro stream
    iFrames &
    read();

    bool
    selectCallback(
		   Ogg::Packet &
		   );
    
    /// @brief Exception thrown by Neuro::read()
    class InconsistentDTDversion : public Ogg::Exception
    {
    public:
      InconsistentDTDversion(
			     unsigned short	readMajor
			     ,unsigned short	readMinor
			     ,unsigned short	dtdMajor
			     ,unsigned short	dtdMinor
			     ) throw()
	;

      InconsistentDTDversion(
			     const InconsistentDTDversion & ex
			     ) throw()
	: Exception(ex)
      {}

      ~InconsistentDTDversion() throw()
      {}
    };
    
    /// @brief Exception thrown by Neuro::read()
    class InconsistentPacketSize : public Ogg::Exception
    {
    public:
      InconsistentPacketSize(
			     size_t	advertised
			     ,size_t	actual
			     ) throw()
	;

      InconsistentPacketSize(
			     const InconsistentPacketSize & ex
			     ) throw()
	: Exception(ex)
      {}

      ~InconsistentPacketSize() throw()
      {}
    };
    
    /// @brief Exception thrown by Neuro::Neuro
    class PacketSizeTooSmall : public Ogg::Exception
    {
    public:
      PacketSizeTooSmall(
			 size_t		minsz
			 ,size_t	actual
			 ) throw()
	;

      PacketSizeTooSmall(
			 const PacketSizeTooSmall & ex
			 ) throw()
	: Exception(ex)
      {}

      ~PacketSizeTooSmall() throw()
      {}
    };
    
    /// @brief Exception thrown by Neuro::track, Neuro::manifold, Neuro::framesPerPacket
    class NotReadingAnyNeuro : public Ogg::Exception
    {
    public:
      NotReadingAnyNeuro() throw()
	;

      NotReadingAnyNeuro(
			 const NotReadingAnyNeuro & ex
			 ) throw()
	: Exception(ex)
      {}

      ~NotReadingAnyNeuro() throw()
      {}
    };
    
    /// @brief Exception thrown by Neuro::write
    class AlreadyWritingNeuro : public Ogg::Exception
    {
    public:
      AlreadyWritingNeuro() throw()
	;

      AlreadyWritingNeuro(
			  const AlreadyWritingNeuro & ex
			  ) throw()
	: Exception(ex)
      {}

      ~AlreadyWritingNeuro() throw()
      {}
    };
    
    /// @brief Exception thrown by Neuro::read
    class AlreadyReadingNeuro : public Ogg::Exception
    {
    public:
      AlreadyReadingNeuro() throw()
	;

      AlreadyReadingNeuro(
			  const AlreadyReadingNeuro & ex
			  ) throw()
	: Exception(ex)
      {}

      ~AlreadyReadingNeuro() throw()
      {}
    };
    
  };

  //@} neuro

}
#endif
